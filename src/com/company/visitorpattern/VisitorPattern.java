package com.company.visitorpattern;

public class VisitorPattern {
    public interface Router
    {
        public void sendData(char[] data);
        public void acceptData(char[] data);
        public void accept(RouterVisitor v);
    }

    public class DLinkRouter implements Router{

        @Override
        public void sendData(char[] data) {
        }

        @Override
        public void acceptData(char[] data) {
        }

        @Override
        public void accept(RouterVisitor v) {
            v.visit(this);
        }
    }

    public class TPLinkRouter implements Router{

        @Override
        public void sendData(char[] data) {
        }

        @Override
        public void acceptData(char[] data) {
        }

        @Override
        public void accept(RouterVisitor v) {
            v.visit(this);
        }
    }

    public interface RouterVisitor {
        public void visit(DLinkRouter router);
        public void visit(TPLinkRouter router);
    }

    public class LinuxConfigurator implements RouterVisitor{

        @Override
        public void visit(DLinkRouter router) {
            System.out.println("DLinkRouter Configuration for Linux complete !!");
        }

        @Override
        public void visit(TPLinkRouter router) {
            System.out.println("TPLinkRouter Configuration for Linux complete !!");
        }

    }

    public class MacConfigurator implements RouterVisitor{

        @Override
        public void visit(DLinkRouter router) {
            System.out.println("DLinkRouter Configuration for Mac complete !!");
        }

        @Override
        public void visit(TPLinkRouter router) {
            System.out.println("TPLinkRouter Configuration for Mac complete !!");
        }
    }

    public void main(){
        MacConfigurator macConfig = new MacConfigurator();
        LinuxConfigurator linuxConfig = new LinuxConfigurator();
        DLinkRouter dLink = new DLinkRouter();
        dLink.accept(macConfig);
        TPLinkRouter tpLink = new TPLinkRouter();
        tpLink.accept(linuxConfig);
    }

}
