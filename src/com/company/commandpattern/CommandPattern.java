package com.company.commandpattern;

public class CommandPattern {
    public interface Command {
        void execute();
    }

    public class Light {
        public  void turnOn() {
            System.out.println("Light is on");
        }

        public void turnOff() {
            System.out.println("Light is off");
        }
    }
    public class Fan {
        void start() {
            System.out.println("Fan Started..");

        }

        void stop() {
            System.out.println("Fan stopped..");

        }
    }
    public class TurnOffLightCommand implements Command {
        Light light;

        public TurnOffLightCommand(Light light) {
            super();
            this.light = light;
        }
        @Override
        public void execute() {
            System.out.println("Turning off light.");
            light.turnOff();
        }
    }
    public class TurnOnLightCommand implements Command {
        Light light;

        public TurnOnLightCommand(Light light) {
            super();
            this.light = light;
        }

        public void execute() {
            System.out.println("Turning on light.");
            light.turnOn();
        }
    }

    public class StartFanCommand implements Command {
        Fan fan;

        public StartFanCommand(Fan fan) {
            super();
            this.fan = fan;
        }

        public void execute() {
            System.out.println("starting Fan.");
            fan.start();
        }
    }

    public class StopFanCommand implements Command {
        Fan fan;

        public StopFanCommand(Fan fan) {
            super();
            this.fan = fan;
        }

        public void execute() {
            System.out.println("stopping Fan.");
            fan.stop();
        }
    }

    public class Remote {
        Command command;

        public void setCommand(Command command) {
            this.command = command;
        }

        public void buttonPressed() {
            command.execute();
        }
    }

        public void main(String[] args)
        {
            Light livingRoomLight = new Light();

            Fan livingRoomFan = new Fan();

            Light bedRoomLight = new Light();

            Fan bedRoomFan = new Fan();

            Remote remote = new Remote();

            remote.setCommand(new TurnOnLightCommand( livingRoomLight ));
            remote.buttonPressed();

            remote.setCommand(new TurnOnLightCommand( bedRoomLight ));
            remote.buttonPressed();

            remote.setCommand(new StartFanCommand( livingRoomFan ));
            remote.buttonPressed();

            remote.setCommand(new StopFanCommand( livingRoomFan ));
            remote.buttonPressed();

            remote.setCommand(new StartFanCommand( bedRoomFan ));
            remote.buttonPressed();

            remote.setCommand(new StopFanCommand( bedRoomFan ));
            remote.buttonPressed();
        }
    }
