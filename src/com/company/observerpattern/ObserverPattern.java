package com.company.observerpattern;

import java.util.ArrayList;
import java.util.List;

public class ObserverPattern {

    public interface Subject
    {
        public void attach(Observer o);
        public void detach(Observer o);
        public void notifyUpdate(Message m);
    }

    public interface Observer
    {
        public void update(Message m);
    }

    public class MessagePublisher implements Subject {

        private List<Observer> observers = new ArrayList<>();

        @Override
        public void attach(Observer o) {
            observers.add(o);
        }

        @Override
        public void detach(Observer o) {
            observers.remove(o);
        }

        @Override
        public void notifyUpdate(Message m) {
            for(Observer o: observers) {
                o.update(m);
            }
        }
    }


    public class MessageSubscriberOne implements Observer
    {
        @Override
        public void update(Message m) {
            System.out.println("MessageSubscriberOne :: " + m.getMessageContent());
        }
    }
    public class MessageSubscriberTwo implements Observer
    {
        @Override
        public void update(Message m) {
            System.out.println("MessageSubscriberTwo :: " + m.getMessageContent());
        }
    }
    public class MessageSubscriberThree implements Observer
    {
        @Override
        public void update(Message m) {
            System.out.println("MessageSubscriberThree :: " + m.getMessageContent());
        }
    }

    public class Message
    {
        final String messageContent;

        public Message (String m) {
            this.messageContent = m;
        }

        public String getMessageContent() {
            return messageContent;
        }
    }
        public void main()
        {
            MessageSubscriberOne subscriber1 = new MessageSubscriberOne();
            MessageSubscriberTwo subscriber2 = new MessageSubscriberTwo();
            MessageSubscriberThree subscriber3 = new MessageSubscriberThree();

            MessagePublisher publisher = new MessagePublisher();

            publisher.attach(subscriber1);
            publisher.attach(subscriber2);

            publisher.notifyUpdate(new Message("First Message"));

            publisher.detach(subscriber1);
            publisher.attach(subscriber3);

            publisher.notifyUpdate(new Message("Second Message"));
        }
    }


