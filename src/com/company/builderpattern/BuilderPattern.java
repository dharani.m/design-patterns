package com.company.builderpattern;

public class BuilderPattern {
    public class Car{
        private final String name;
        private final int fuelCapacity;
        private final String fuelType;
        private final int range;
        private final String engine;

        private Car(CarBuilder cb){
            this.name = cb.name;
            this.fuelType = cb.fuelType;
            this.fuelCapacity = cb.fuelCapacity;
            this.range = cb.range;
            this.engine = cb.engine;
        }
        public String getName() {
            return name;
        }
        public int getFuelCapacity() {
            return fuelCapacity;
        }
        public String getFuelType() {
            return fuelType;
        }
        public int getRange() {
            return range;
        }
        public String getEngine() {
            return engine;
        }

        public void drive(){}
        public void fillFuel(){}

        public void displayDetails() {
            System.out.printf("Name: %s\nFuel Capacity: %d\nFuel Type: %s\nRange: %s\nEngine: %s",this.name,this.fuelCapacity,this.fuelType,this.range,this.range);
        }
    }

    public class CarBuilder{
        private final String name;
        private int fuelCapacity;
        private String fuelType;
        private int range;
        private String engine;

        public CarBuilder(String name){
            this.name = name;
        }
        public CarBuilder setFuelCapacity(int capacity){
            this.fuelCapacity = capacity;
            return this;
        }
        public CarBuilder setFuelType(String fuelType){
            this.fuelType = fuelType;
            return this;
        }
        public CarBuilder setRange(int range){
            this.range = range;
            return this;
        }
        public CarBuilder setEngine(String engine){
            this.engine = engine;
            return this;
        }
        public Car build(){
            return new Car(this);
        }
    }

    public void main(){
        CarBuilder tiagoBuilder = new CarBuilder("Tiago");
        tiagoBuilder.setFuelCapacity(40);
        tiagoBuilder.setFuelType("petrol");
        Car tiago = tiagoBuilder.build();
        tiago.displayDetails();
        System.out.println("\n----------------------");
        Car tigor = new CarBuilder("Tigor").setFuelType("Diesel").setRange(60).setEngine("Chervotron").build();
        tigor.displayDetails();
    }
}
