package com.company.singleton;

public class Singleton {
    public static class EagerInitialization{
        private static Singleton instance = new Singleton();

        private EagerInitialization(){

        }
        public static Singleton getInstance(){
            return instance;
        }
    }

    public static class LazyInitialization{
        private static volatile LazyInitialization instance = null;

        // private constructor
        private LazyInitialization() {
        }

        public static LazyInitialization getInstance() {

            if (instance == null) {
                synchronized (LazyInitialization.class) {
                    instance = new LazyInitialization();
                }
            }
            return instance;
        }
    }
}
