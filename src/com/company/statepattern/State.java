package com.company.statepattern;

public class State {

    public interface SmartTvState
    {
        public void switchTvState();
    }

    public class SmartTvSwitchOffState implements SmartTvState {
        @Override
        public void switchTvState()
        {
            System.out.println("Smart TV is Switched OFf");
        }
    }

    public class SmartTvSwitchOnState implements SmartTvState
    {
        public void goToChannel(){

        }
        @Override
        public void switchTvState() {
            System.out.println("Smart TV is Switched On");
        }
    }

    public class SmartTv implements SmartTvState{
        private SmartTvState smartTvState;
        private String state = "OFF";

        public SmartTvState getSmartTvState() {
            return smartTvState;
        }

        public void setSmartTvState(SmartTvState smartTvState) {
            this.smartTvState = smartTvState;

        }

        @Override
        public void switchTvState() {
            System.out.println("Current state Of Smart Tv : " +
                    smartTvState.getClass().getName());
            smartTvState.switchTvState();
        }
    }

    public void main()
        {
            SmartTv smartTv= new SmartTv();
            SmartTvState smartTvSwitchOnState = new SmartTvSwitchOnState();
            SmartTvState smartTvSwitchOffState = new SmartTvSwitchOffState ();

            smartTv.setSmartTvState(smartTvSwitchOnState);
            smartTv.switchTvState();

            smartTv.setSmartTvState(smartTvSwitchOffState);
            smartTv.switchTvState();
        }

    }

