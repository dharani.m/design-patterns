package com.company.decoratorpattern;

public class DecoratorPattern {
    public abstract class Pizza{
        String description = "Basic Pizza";

        public String getDescription(){
            return description;
        }
        public abstract double cost();
    }

    public class ThinCrustPizza extends Pizza{

        public ThinCrustPizza(){
            description = "Thin crust pizza";
        }
        @Override
        public double cost() {
            return 5.00;
        }
    }

    public class ThickCrustPizza extends Pizza{

        public ThickCrustPizza(){
            description = "Thick crust pizza";
        }
        @Override
        public double cost() {
            return 4.50;
        }
    }

    public abstract class ToppingsDecorator extends Pizza{
        Pizza pizza;
        public abstract String getDescription();
    }

    public class Cheese extends ToppingsDecorator{

        public Cheese(Pizza pizza){
            this.pizza = pizza;
        }
        @Override
        public double cost() {
            return pizza.cost();
        }

        @Override
        public String getDescription() {
            return pizza.getDescription() + ", with Cheese";
        }
    }

    public class Olives extends ToppingsDecorator{

        public Olives(Pizza pizza){
            this.pizza = pizza;
        }
        @Override
        public double cost() {
            return pizza.cost()+.5;
        }

        @Override
        public String getDescription() {
            return pizza.getDescription() + ", with Olives";
        }
    }

    public void main(){
        Pizza pizza = new ThickCrustPizza();
        Pizza cheesePizza = new Cheese(pizza);
        Pizza greekPizza = new Olives(cheesePizza);

        System.out.println(greekPizza.getDescription()+" : $"+greekPizza.cost());
    }
}
