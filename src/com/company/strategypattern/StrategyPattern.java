package com.company.strategypattern;

public class StrategyPattern {
    public interface UpgradableOS{
        public void upgradeOS();
    }
    public class Phone{
        private UpgradableOS upgradableOS;
        public Phone(UpgradableOS upgradeOS){
            this.upgradableOS = upgradeOS;
        }
        public void upgradeOS(){
            this.upgradableOS.upgradeOS();
        }
    }
    public class AndroidOS implements UpgradableOS{
        private String OS;
        @Override
        public void upgradeOS() {
            this.OS = "Android 10";
        }
    }
    public class IOS implements UpgradableOS{
        private String OS;

        @Override
        public void upgradeOS() {
            this.OS = "IOS 14";
        }
    }
    public void main(){
        Phone iphone9 = new Phone(new IOS());
        Phone vivoV15 = new Phone(new AndroidOS());
        iphone9.upgradeOS();
        vivoV15.upgradeOS();
    }
}
