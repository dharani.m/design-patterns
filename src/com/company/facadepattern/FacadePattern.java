package com.company.facadepattern;


public class FacadePattern {

    public class Report {

        private ReportHeader header;
        private ReportData data;
        private ReportFooter footer;

        public ReportHeader getHeader() {
            return header;
        }
        public void setHeader(ReportHeader header) {
            System.out.println("Setting report header");
            this.header = header;
        }
        public ReportData getData() {
            return this.data;
        }
        public void setData(ReportData data) {
            System.out.println("Setting report data");
            this.data = data;
        }
        public ReportFooter getFooter() {
            return footer;
        }
        public void setFooter(ReportFooter footer) {
            System.out.println("Setting report footer");
            this.footer = footer;
        }
    }
    public class ReportHeader {
        private String header;
        public ReportHeader(String header) {
            this.header = header;
        }
    }
    public class ReportFooter {
        private String footer;
        public ReportFooter(String footer) {
            this.footer = footer;
        }
    }
    public class ReportData {
        private String data;
        public ReportData(String data) {
            this.data = data;
        }
        public String getData(){
            return this.data;
        }
    }
    public enum ReportType
    {
        PDF, HTML
    }
    public class ReportWriter {

        public void writeHtmlReport(Report report, String location) {
            System.out.println("HTML Report written in location : "+location);
            System.out.println("Data:\n"+report.getData().data);
        }

        public void writePdfReport(Report report, String location) {
            System.out.println("Pdf Report written in location : "+location);
            System.out.println("Data:\n"+report.getData().data);
        }
    }

    public class ReportGeneratorFacade
    {
        public void generateReport(ReportType type, String[] data, String location)
        {
            Report report = new Report();

            report.setHeader(new ReportHeader(data[0]));
            report.setFooter(new ReportFooter(data[1]));

            report.setData(new ReportData(data[2]));

            ReportWriter writer = new ReportWriter();
            switch(type)
            {
                case HTML:
                    writer.writeHtmlReport(report, location);
                    break;

                case PDF:
                    writer.writePdfReport(report, location);
                    break;
            }
        }
    }

        public void main()
        {
            ReportGeneratorFacade reportGeneratorFacade = new ReportGeneratorFacade();

            reportGeneratorFacade.generateReport(ReportType.HTML, new String[]{"header 1", "footer 1", "data 1"}, "c://folder1");
            reportGeneratorFacade.generateReport(ReportType.PDF, new String[]{"header 2", "footer 2", "data 2"}, "d://folder2");
        }
    }


