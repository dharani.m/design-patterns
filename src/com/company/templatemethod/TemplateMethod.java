package com.company.templatemethod;

import java.util.ArrayList;

public class TemplateMethod {
    public abstract class IcecreamTemplate{
        public String flavour;
        public ArrayList<String> toppings = new ArrayList<>();
        abstract void setFlavour();
        abstract void addToppings();
        abstract void addSyrup();
        public void displayIceCreamDetails(){
            System.out.println("Flavour: "+this.flavour);
            System.out.println("Toppings: "+this.toppings.toString());
        }
        public void createIceCream(){
            setFlavour();
            addToppings();
            addSyrup();
        }
    }
    public class VanillaChocoDelight extends IcecreamTemplate{

        @Override
        void setFlavour() {
            this.flavour = "vanilla";
        }

        @Override
        void addToppings() {
            this.toppings.add("Chocolate chips");
        }

        @Override
        void addSyrup() {
            this.toppings.add("Chocolate syrup");
        }

    }
    public class ButterScotchSundae extends IcecreamTemplate{

        @Override
        void setFlavour() {
            this.flavour = "Butter scotch";
        }

        @Override
        void addToppings() {
            this.toppings.add("Butterscotch crystals");
        }

        @Override
        void addSyrup() {
            this.toppings.add("Butterscotch syrup");
        }
    }

    public void main(){
        IcecreamTemplate order1 = new VanillaChocoDelight();
        IcecreamTemplate order2 = new ButterScotchSundae();
        order1.createIceCream();
        order2.createIceCream();
        order1.displayIceCreamDetails();
        order2.displayIceCreamDetails();
    }
}
