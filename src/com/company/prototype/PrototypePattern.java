package com.company.prototype;

public class PrototypePattern {
    public interface Prototype{
        public Prototype clone();
    }
    public class Student implements Prototype{
        private int rollNo;
        private String name;
        private String department;

        public Student(int rollNo, String name, String department){
            this.rollNo = rollNo;
            this.name = name;
            this.department = department;
        }

        public void displayStudentDetails(){
            System.out.printf("\n<--- Student Details --->\nName : %s\nRoll No. : %d\nDepartment : %s\n",this.name,this.rollNo,this.name);
        }
        @Override
        public Prototype clone() {
            return new Student(this.rollNo,this.name,this.department);
        }
    }

    public class Professor implements Prototype{
        private int id;
        private String name;
        private String department;

        public Professor(int id, String name, String department){
            this.id = id;
            this.name = name;
            this.department = department;
        }

        public void displayProfessorDetails(){
            System.out.printf("\n<--- Professor Details --->\nName : %s\nID : %d\nDepartment : %s\n",this.name,this.id,this.name);
        }
        @Override
        public Prototype clone() {
            return new Student(this.id,this.name,this.department);
        }
    }

    public void main(){
        Student stu1 = new Student(25,"Xyz","CSE");
        Student stu1Clone = (Student) stu1.clone();
        stu1.displayStudentDetails();
        stu1Clone.displayStudentDetails();
    }
}
