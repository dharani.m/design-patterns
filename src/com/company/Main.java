package com.company;

import com.company.builderpattern.BuilderPattern;
import com.company.decoratorpattern.DecoratorPattern;
import com.company.facadepattern.FacadePattern;
import com.company.observerpattern.ObserverPattern;
import com.company.prototype.PrototypePattern;
import com.company.statepattern.State;
import com.company.templatemethod.TemplateMethod;
import com.company.visitorpattern.VisitorPattern;

public class Main {

    public static void main(String[] args) {
        BuilderPattern b = new BuilderPattern();
        b.main();
        new PrototypePattern().main();
        new ObserverPattern().main();
        new FacadePattern().main();
        new DecoratorPattern().main();
        new TemplateMethod().main();
        new VisitorPattern().main();
        new State().main();
    }
}
